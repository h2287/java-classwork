package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input you nick: ");
		String nickName = sc.nextLine();

		boolean correctNickName = true;

		String tempName = nickName.toUpperCase();
		char[] sym = tempName.toCharArray();
		for (int i = 0; i < sym.length; i++) {
			if (!((sym[i] >= 'A' && sym[i] <= 'Z') || sym[i] == '_')) {
				correctNickName = false;
				break;
			}
		}

		if (correctNickName) {
			System.out.println(nickName + " correct");
		} else {
			System.out.println(nickName + " incorrect");
		}

	}

}