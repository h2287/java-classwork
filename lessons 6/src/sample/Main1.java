package sample;

import java.util.Scanner;

public class Main1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input you code: ");
		String code = sc.nextLine();

		int number = 0;
		char[] sym = code.toCharArray();
		for (int i = 0; i < sym.length; i++) {
			if (sym[i] == '{') {
				number += 1;
			}
			if (sym[i] == '}') {
				number -= 1;
			}
		}
		if (number == 0) {
			System.out.println("Code balanced. ");
		} else {
			System.out.println("Code unbalanced. ");

		}

	}

}
