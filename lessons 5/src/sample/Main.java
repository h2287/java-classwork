package sample;

import java.util.Arrays;
import java.util.Random;

public class Main {

	public static void main(String[] args) {

//		int[] myArray = new int[] { 7, 4, 8, 0 };

//		int sum = 0;
//
//		for (int i = 0; i < myArray.length; i++) {
//			sum = sum + myArray[i];
//		}
//		
//		for (int element : myArray) {
//			sum = sum + element;
//		}
//
//		System.out.println(sum);
//		System.out.println(myArray.getClass());
//	System.out.println(myArray);

//		int[] copyArray = Arrays.copyOfRange(myArray, 1, 4);
//
//		System.out.println(Arrays.toString(myArray));
//
//		System.out.println("copy = " + Arrays.toString(copyArray));
//
//		Arrays.sort(copyArray);
//
//		System.out.println("copy = " + Arrays.toString(copyArray));

		Random rn = new Random();

		int[] a = new int[10];

		for (int i = 0; i < a.length; i++) {
			a[i] = rn.nextInt(10);
		}
		System.out.println(Arrays.toString(a));

		for (int i = 0; i < a.length; i++) {
			if (a[i] > 5) {
				a[i] = 0;
			}
		}
		System.out.println(Arrays.toString(a));
	}

}
