package sample;

import java.io.File;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {

		File file1 = new File("new_file.txt");

		try {
			file1.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		File folder1 = new File("AAA");

		folder1.mkdirs();

		File file2 = new File(folder1, "bbb.docx");

		try {
			file2.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		file1.delete();
		file2.delete();
		folder1.delete();

		File workFolder = new File(".");
		File[] files = workFolder.listFiles();
		for (File file : files) {
			String type = "File";
			long fileSize = file.length();
			if (file.isDirectory()) {
				type = "Folder";
			}
			System.out.println(file + "\t" + type + "\t" + fileSize);
		}

	}

}
