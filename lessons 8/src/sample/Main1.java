package sample;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class Main1 {

	public static void main(String[] args) {
		File file=new File("hello.txt");
		
		try(PrintWriter pw=new PrintWriter(file)){
			pw.println("Hello world");
		}catch (IOException e) {
			e.printStackTrace();
		}

	}

}
