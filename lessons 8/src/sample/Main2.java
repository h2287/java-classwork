package sample;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main2 {

	public static void main(String[] args) {
//	    int[] array = new int[] { 4, 7, 9, -10, 0 };
	    File file = new File("array.txt");
//	    saveArrayToFile(file, array);
	    
	    String result = getStringFromFile(file);
	    
	    System.out.println(result);
	  }

	  public static void saveArrayToFile(File file, int[] arr) {
	    try (PrintWriter pw = new PrintWriter(file)) {
	      for (int i = 0; i < arr.length; i++) {
	        pw.println(arr[i]);
	      }
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	  }

	  public static String getStringFromFile(File file) {
	    String result = "";
	    try (Scanner sc = new Scanner(file)) {

	      for (; sc.hasNextLine();) {
	        result += sc.nextLine() + System.lineSeparator();
	      }

	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	    return result;
	  }

	}
