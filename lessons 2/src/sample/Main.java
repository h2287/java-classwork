package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input coffee cost (UAN): ");
		int coffeCost = sc.nextInt();
		System.out.println("Input coffee cups: ");
		int cups = sc.nextInt();
		int sum;

		sum = coffeCost * cups;

		System.out.println(sum + " UAH");
	}
}