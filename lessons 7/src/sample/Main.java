package sample;

public class Main {

	public static void main(String[] args) {
		int[] myArr = new int[] { -2, 3, 7 };
		double[] arr = new double[] { 1.5, 0.5, 2.45 };

//		int sum = 0;
//		for (int i = 0; i < myArr.length; i++) {
//			sum += myArr[i];
//		}
//		System.out.println(sum);

		int s = calculateArrayElementSum(myArr);

		System.out.println(s);

		double s1 = calculateArrayElementSum(arr);

		System.out.println(s1);

		printString(":) ", 1);
	}

	public static int calculateArrayElementSum(int[] array) {
		int sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		return sum;
	}

	public static double calculateArrayElementSum(double[] array) {
		double sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		return sum;
	}

	public static void printString(String text, int n) {
		for (int i = 0; i <= n; i++) {
			System.out.println(text);
		}
		System.out.println();
	}
}
