package sample;

public class Main2 {

	public static void main(String[] args) {
		System.out.println(calculateFactorial(14));
	}

	public static long calculateFactorial(int n) {
		long factorial = 1;
		for (int i = 1; i <= n; i++) {
			factorial = factorial * i;
		}
		return factorial;
	}

}
