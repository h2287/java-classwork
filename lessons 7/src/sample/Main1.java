package sample;

import java.util.Scanner;

public class Main1 {

	public static void main(String[] args) {

		String someText = "Hello java word";

		System.out.println(getLongesWord(someText));

	}

	public static String getLongesWord(String text) {
		String[] words = text.split("[ ]");
		String longeWord = words[0];
		for (int i = 0; i < words.length; i++) {
			if (words[i].length() > longeWord.length()) {
				longeWord = words[i];
			}
		}
		return longeWord;
	}

}
